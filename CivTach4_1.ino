//Civic Tach Program
//Rev 4.2 Aug 19 2014
//Nigel Myers

#include <SoftwareSerial.h>

#define SerIn 4 // Set pins
#define SerOut 11
#define TachIn 2
#define GreenPin 12
#define RedPin 10
#define DimPin 5

SoftwareSerial MySerial(SerIn, SerOut); //Set Serial Settings

unsigned long pulse_time[5]; //Pulse gap array
float RPM = 0; //RPM value
unsigned long pulse_last = 0; //Time of last pulse
unsigned long refreshtime = 0;
unsigned long refreshchk = 0;
int jitter_last = 0; //Last jitter data
int jitter_last_delta = 0; //Last jitter change
int DimOn = 0;
int count = 0;
int count2 = 0;
float pulseAvg = 0;
float pulseAvg2 = 0;
int wait = 0;
int off = 1;
uint8_t zero = 0;

void setup() {
	pinMode(SerOut, OUTPUT); //Set pins as input or output
	pinMode(SerIn, INPUT);
	pinMode(TachIn, INPUT);
	pinMode(GreenPin, OUTPUT);
	pinMode(RedPin, OUTPUT);
	pinMode(DimPin, INPUT);
	digitalWrite(DimPin, HIGH);
	digitalWrite(GreenPin, HIGH);
	digitalWrite(RedPin, HIGH);

	MySerial.begin(9600); //Initiate serial com
	MySerial.write("v"); //Reset Display

	MySerial.write("w");
	MySerial.write(zero);
	pulse_time[0] = 9999999;
	pulse_time[1] = 9999999;
	pulse_time[2] = 9999999;
	pulse_time[3] = 9999999;
	pulse_time[4] = 9999999;


	if ((digitalRead(DimPin)) == LOW) {
		MySerial.write("z");
		MySerial.write(50);
		DimOn = 1;
	}
	else {
		MySerial.write("z");
		MySerial.write(zero);
	}

	MySerial.print("----");
	delay(300);
	MySerial.print("xxxx");
	digitalWrite(GreenPin, LOW);
	digitalWrite(RedPin, HIGH);
	delay(300);
	MySerial.print("----");
	digitalWrite(GreenPin, HIGH);
	digitalWrite(RedPin, HIGH);
	delay(300);
	MySerial.print("xxxx");
	digitalWrite(GreenPin, HIGH);
	digitalWrite(RedPin, LOW);
	delay(300);
	MySerial.print("0000");
	digitalWrite(GreenPin, LOW);
	digitalWrite(RedPin, LOW);

	delay(500);
	digitalWrite(GreenPin, HIGH);
	digitalWrite(RedPin, HIGH);
	MySerial.write("v");
	MySerial.print("x0FF");
	attachInterrupt(0, start_new, RISING); //Detect RISING Value
}

void loop() {
	refreshchk = (millis() - refreshtime);
	if ((refreshchk>100) && (off == 0)){
		refresh();
		refreshtime = millis();
	}

}

void start_new() { //Reset timer with more accurate value
	detachInterrupt(0);
	pulse_last = micros(); //Reset Timer
	refreshtime = millis();
	off = 0;
	attachInterrupt(0, rpm_calc, RISING); //Restart interrupt
}

void rpm_calc() { //Calc RPM value
//	detachInterrupt(0);
	pulse_time[count] = (micros() - pulse_last);
	if (pulse_time[count]>3500){
		if (count == 4){
			count = 0;
		}
		else {
			count++;
		}
		pulse_last = micros();
	}
//	attachInterrupt(0, rpm_calc, RISING); //Restart interrupt
}

int jitter(int current, int filter_size) { //Directional filtering for RPM
	int jitter_delta = 0; // Difference
	int delta_unsigned = 0; //Magnitude
	int jitter_match = 0; //criteria met identifier
	int jitter_result = 0; //Filtered value

	if (current == jitter_last){ //If same, send same
		jitter_result = current;
		jitter_delta = 0;
		jitter_match = 1;
	}

	if (!(jitter_match)) { //Determine difference and magnitude
		jitter_delta = current - jitter_last;
		delta_unsigned = abs(jitter_delta);
	}

	if (!(jitter_match)){ //If last was lower and this was higher, ignore value unless over threshold
		if (jitter_delta < 0){
			if (jitter_last_delta > 0){
				if (delta_unsigned < filter_size){
					jitter_result = jitter_last;
					jitter_match = 1;
				}
			}
		}
	}

	if (!(jitter_match)){ //If last was higher and this was lower, ignore value unless over threshold
		if (jitter_delta > 0){
			if (jitter_last_delta < 0){
				if (delta_unsigned < filter_size){
					jitter_result = jitter_last;
					jitter_match = 1;
				}
			}
		}
	}

	if (!(jitter_match)) { //Send new value as direction was the same
		jitter_result = current;
		jitter_match = 1;

		if (current != jitter_last){ //Set values for next iteration
			jitter_last = current;
			jitter_last_delta = jitter_delta;
		}
	}
	return jitter_result; //Send Value
}

void printRPM(int rpm) { //Sends data to display
	//detachInterrupt(0);
	if ((rpm < 1000) && (rpm >= 100)) {
		MySerial.print("x"); //If value under 1000 then insert blank char
		MySerial.print(rpm);
	}
	else
	if ((rpm <= 9999) && (rpm >= 100)){
		MySerial.print(rpm); //Else print normally
	}
	else {
		MySerial.print("xxx0");
	}
}

void refresh() {
	if (((digitalRead(DimPin)) == LOW) && (DimOn == 0)) {
		MySerial.write("z");
		MySerial.write(50);
		DimOn = 1;
	}
	else if (((digitalRead(DimPin)) == HIGH) && (DimOn == 1)) {
		MySerial.write("z");
		MySerial.write(zero);
		DimOn = 0;
	}
	pulseAvg = (float)(pulse_time[0] + pulse_time[1] + pulse_time[2] + pulse_time[3] + pulse_time[4]) / 5000.0;
	if (abs(pulseAvg - pulseAvg2)<.001){
		count2++;
	}
	else{
		count2 = 0;
	}
	RPM = (30000.0) / pulseAvg;
	RPM = jitter(RPM, 50);
	if (RPM > 6400) {
		digitalWrite(RedPin, LOW); //Redline
	}
	else {
		digitalWrite(RedPin, HIGH);
		if (RPM > 6000) { //Shift point
			digitalWrite(GreenPin, LOW);
		}
		else {
			digitalWrite(GreenPin, HIGH);
		}
	}
//	detachInterrupt(0);
	if (count2 > 10){
		MySerial.print("x0FF");
		off = 1;
		detachInterrupt(0);
		attachInterrupt(0, start_new, RISING);
	}
	else{
		printRPM((int)RPM);
	}
	//attachInterrupt(0, start_new, RISING);
	pulseAvg2 = pulseAvg;
}